package ru.fnight.etis.domain;

import com.petersamokhin.bots.sdk.clients.Group;
import org.springframework.beans.factory.annotation.Autowired;

public class VkBot {

    private Group group;

    @Autowired
    public VkBot(String token) {
        this.group = new Group(token);
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

}
