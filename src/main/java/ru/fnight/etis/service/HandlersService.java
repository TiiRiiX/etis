package ru.fnight.etis.service;

import com.petersamokhin.bots.sdk.callbacks.messages.OnSimpleTextMessageCallback;

public interface HandlersService {

    OnSimpleTextMessageCallback simpleTextMessageCallback();

}
