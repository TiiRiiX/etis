package ru.fnight.etis.service;

import ru.fnight.etis.domain.VkBot;

public interface VkBotService {

    VkBot getVkBot();
}
