package ru.fnight.etis.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.fnight.etis.domain.VkBot;
import ru.fnight.etis.service.HandlersService;
import ru.fnight.etis.service.VkBotService;

import javax.annotation.PostConstruct;

@Service
public class VkBotServiceImpl implements VkBotService {

    @Autowired
    private HandlersService handlersService;

    @Value("${token}")
    private String token;

    private VkBot vkBot;

    @PostConstruct
    public void init() {
        vkBot = new VkBot(token);
        vkBot.getGroup().onSimpleTextMessage(handlersService.simpleTextMessageCallback());
    }

    @Override
    public VkBot getVkBot() {
        return vkBot;
    }

}
