package ru.fnight.etis.service.impl;

import com.petersamokhin.bots.sdk.callbacks.messages.OnSimpleTextMessageCallback;
import com.petersamokhin.bots.sdk.objects.Message;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.fnight.etis.service.HandlersService;
import ru.fnight.etis.service.VkBotService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class HandlersServiceImpl implements HandlersService {

    @Autowired
    private VkBotService botService;

    @Override
    public OnSimpleTextMessageCallback simpleTextMessageCallback() {

        return message -> {
            Document document;
            List<String> lessons = new ArrayList();
            try {

                Response login = Jsoup.connect("https://student.psu.ru/pls/stu_cus_et/stu.login")
                        .header("Content-Type", "application/x-www-form-urlencoded")
                        .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                        .header("Accept-Encoding", "gzip, deflate, br")
                        .header("Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7")
                        .requestBody("p_redirect=&p_username=%D1%F3%F1%E0%ED%EE%E2&p_password=IIaXaH0852")
                        .method(Method.POST)
                        .execute();

                document = Jsoup.connect("https://student.psu.ru/pls/stu_cus_et/stu.timetable")
                        .cookies(login.cookies())
                        .get();


                document.select(".timetable").select(".pair_info").select(".dis").select("a").forEach(element ->
                        lessons.add(element.text()));

                System.out.println();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
            new Message()
                    .from(botService.getVkBot().getGroup())
                    .to(message.authorId())
                    .text(String.join("\n\n", lessons))
                    .send();
        };
    }

}
