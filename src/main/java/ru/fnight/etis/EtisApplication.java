package ru.fnight.etis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EtisApplication {

    public static void main(String[] args) {
        SpringApplication.run(EtisApplication.class, args);
    }

}

